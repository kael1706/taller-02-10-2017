<?php


class pruebaBTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testB1()
    {
        $alien = AlienFactory::getAlien("Ancient",9999,"Primerus Serus","Sun");
        $jalien = AlienFactory::getJupiterAlien("Jane",212,"Jupiterus Corvus");
        $malien = AlienFactory::getMarsAlien("Marcus",312,"Martianus Mexumos");
        $moalien = AlienFactory::getMoonAlien("Mulen",123,"Lunaticus");
        $palien = AlienFactory::getPlutoAlien("Pluterus",123,"Plutiretus Minus");
        $salien = AlienFactory::getSaturnAlien("Sandros",12,"Saturicus Olus");
        $valien = AlienFactory::getVenusAlien("Valerixolus",39,"Seductorus Facilus");

        $this->assertEquals("telepaticamente dice: \r\n    Mi nombre es Ancient, vengo del plantea Sun, soy un Primerus Serus y soy neutral",$alien->whoIAm());
        $this->assertEquals("telepaticamente dice: \r\n    Mi nombre es Jane, vengo del plantea Jupiter, soy un Jupiterus Corvus y soy malo",$jalien->whoIAm());
        $this->assertEquals("telepaticamente dice: \r\n    Mi nombre es Marcus, vengo del plantea Marte, soy un Martianus Mexumos y soy bueno",$malien->whoIAm());
        $this->assertEquals("telepaticamente dice: \r\n    Mi nombre es Mulen, vengo del plantea Moon, soy un Lunaticus y soy bueno",$moalien->whoIAm());
        $this->assertEquals("telepaticamente dice: \r\n    Mi nombre es Pluterus, vengo del plantea Pluto, soy un Plutiretus Minus y soy malo",$palien->whoIAm());
        $this->assertEquals("telepaticamente dice: \r\n    Mi nombre es Sandros, vengo del plantea Saturn, soy un Saturicus Olus y soy malo",$salien->whoIAm());
        $this->assertEquals("telepaticamente dice: \r\n    Mi nombre es Valerixolus, vengo del plantea Venus, soy un Seductorus Facilus y soy bueno",$valien->whoIAm());
        
        $this->assertEquals("telepaticamente dice: Hola terricola mi nombre es Ancient, vinimos en son de paz",$alien->interact());
        $this->assertEquals("telepaticamente dice: Hola terricola, rindanshe ante la invashión de   Jane",$jalien->interact());
        $this->assertEquals("telepaticamente dice: Hola terricola mi nombre es Marcus, vinimos en son de paz",$malien->interact());
        $this->assertEquals("telepaticamente dice: Hola terricola mi nombre es Mulen, vinimos en son de paz",$moalien->interact());
        $this->assertEquals("telepaticamente dice: Hola terricola, rindanse ante la invasión de   Pluterus",$palien->interact());
        $this->assertEquals("telepaticamente dice: Hola terricola, rindanse ante la invasión de   Sandros",$salien->interact());
        $this->assertEquals("telepaticamente dice: Hola terricola mi nombre es Valerixolus, hmmm, vinimos en son de paz",$valien->interact());

        $this->assertEquals("Solicitando refuerzos Muahahaha",$palien->pedirRefuerzos());
        $this->assertEquals("Llamando a Caaaaasaaaaa",$valien->llamarACasa());

        $planet = new Planet("tierra");
        
        $this->assertEquals("a salvo",$planet->status());
        $palien->destruirPlaneta($planet);
        $this->assertEquals("destruido",$planet->status());
        $valien->salvarPlaneta($planet);
        $this->assertEquals("a salvo",$planet->status());
    }
}