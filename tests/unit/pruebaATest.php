<?php

spl_autoload_register(function($class) {
    if (file_exists("entities/" . $class . ".php")) {
        include "entities/" . $class . ".php";
        return false;
    }
    if (file_exists("factories/" . $class . ".php")) {
        include "factories/" . $class . ".php";
        return false;
    }
    if (file_exists("interfaces/" . $class . ".php")) {
        include "interfaces/" . $class . ".php";
        return false;
    }
    if (file_exists("bridges/" . $class . ".php")) {
        include "bridges/" . $class . ".php";
        return false;
    }
});
class pruebaATest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testA1()
    {
        $e=true;
        try{
        
        $alien = AlienFactory::getAlien("Ancient",9999,"Primerus Serus","Sun");
        $alien->interact();
        $alien->whoIAm();

        $jalien = AlienFactory::getJupiterAlien("Jane",212,"Jupiterus Corvus");
        $jalien->interact();
        $jalien->whoIAm();

        $malien = AlienFactory::getMarsAlien("Marcus",312,"Martianus Mexumos");
        $malien->interact();
        $malien->whoIAm();

        $moalien = AlienFactory::getMoonAlien("Mulen",123,"Lunaticus");
        $moalien->interact();
        $moalien->whoIAm();

        $palien = AlienFactory::getPlutoAlien("Pluterus",123,"Plutiretus Minus");
        $palien->interact();
        $palien->whoIAm();

        $salien = AlienFactory::getSaturnAlien("Sandros",12,"Saturicus Olus");
        $salien->interact();
        $salien->whoIAm();

        $valien = AlienFactory::getVenusAlien("Valerixolus",39,"Seductorus Facilus");
        $valien->interact();
        $valien->whoIAm();
        
        $planet = new Planet("tierra");
        $planet->status();

        }
        
        catch(Exception $e){
        
            $e=false;
        
        }
          
        $this->assertTrue(true);
    }
}