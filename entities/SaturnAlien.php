<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SaturnAlien
 *
 * @author pabhoz
 */
class SaturnAlien extends BadAlien{
    private $nombre, $edad, $especie;
    private $planeta = "Saturn";
    
    function __construct($nombre, $edad, $especie) {
        parent::__construct($nombre, $edad, $especie, $this->planeta);
    }
}
