<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pabhoz
 */
interface IAlienFactory {
    public function getAlien($nombre, $edad, $especie, $planeta): Alien;
    public function getJupiterAlien($nombre, $edad, $especie): JupiterAlien;
    public function getMarsAlien($nombre, $edad, $especie): MarsAlien;
    public function getMoonAlien($nombre, $edad, $especie): MoonAlien;
    public function getPlutoAlien($nombre, $edad, $especie): PlutoAlien;
    public function getSaturnAlien($nombre, $edad, $especie): SaturnAlien;
    public function getVenusAlien($nombre, $edad, $especie): VenusAlien;
}
